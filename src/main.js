// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import { store } from "./store/store";

Vue.component('v-select', vSelect)

Vue.config.productionTip = false

Vue.use(Vuesax)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
