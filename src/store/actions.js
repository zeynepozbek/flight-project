export const updateFlightInfo = ({ commit }, payload) => {
  commit('UPDATE_FLIGHT_INFO', payload)
};
