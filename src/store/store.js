import Vue from "vue";
import Vuex from "vuex";
import * as actions from "./actions";
import * as mutations from "./mutations";
import * as getters from "./getters";

Vue.use(Vuex);

let flightInfo = null;
var localCheck = localStorage.getItem('flightInfo')

if(!localCheck){
  localStorage.setItem('flightInfo',JSON.stringify(flightInfo))
}
else {
  flightInfo = JSON.parse(localCheck)
}

export const store = new Vuex.Store({
  state: {
    flightInfo: flightInfo
  },
  actions,
  mutations,
  getters
});
