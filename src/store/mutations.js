export const UPDATE_FLIGHT_INFO = (state, payload) => {
  localStorage.setItem('flightInfo',JSON.stringify(payload))
  state.flightInfo = payload;
};
