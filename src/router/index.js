import Vue from 'vue'
import Router from 'vue-router'
import Search from '@/components/Search'
import List from '@/components/List'
import Result from '@/components/Result'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Search',
      component: Search
    },
    {
      path: '/list',
      name: 'List',
      component: List
    },
    {
      path: '/result',
      name: 'Result',
      component: Result
    },
  ]
})
