import Vue from 'vue'
import Search from '@/components/Search'
import List from '@/components/List'

describe('Search.vue', () => {

  const Constructor = Vue.extend(Search)
  const vm = new Constructor().$mount()

  it('should render correct contents', () => {
    expect(vm.$el.querySelector('.search-page__title').textContent)
      .to.equal('Merhaba')
  })

})

describe('List.vue', () => {

  const Constructor = Vue.extend(List)
  const vm = new Constructor().$mount()

  it('should be instantiated', () => {
    expect(vm.$el.firstChild.tagName).to.equal('VS-ROW');
    expect(vm.$el.lastChild.tagName).to.equal('VS-ROW');
  });

})

